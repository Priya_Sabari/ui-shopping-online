getOrders()
//this method used to get order details of users
 function getOrders()
{
    let httpReq; 
    if(window.XMLHttpRequest)
    {
        httpReq = new XMLHttpRequest();
    }
    else{
         httpReq  = new ActiveXObject();
    }
    // api call response handling 
    httpReq.onreadystatechange = function() 
    {
  if(this.readyState===4 && this.status===200)
  {
    //its used for the creation of table heading
    let ordersHeading =  document.createElement("h2");
    ordersHeading.appendChild(document.createTextNode("My Orders"));
    ordersHeading.setAttribute("id",'headingID');

      let table = document.createElement('table');
      table.setAttribute("id",'dataTable');
      
      let thead = document.createElement('thead');
      let tbody = document.createElement('tbody');

      let headTr = document.createElement('tr');

      let td1 = document.createElement('td');
      let td1Text = document.createTextNode("Product ID");
      td1.appendChild(td1Text);

      let td2 = document.createElement('td');
      let td2Text = document.createTextNode("Quantity");
      td2.appendChild(td2Text);

      let td3 = document.createElement('td');
      let td3Text = document.createTextNode("Unit Price");
      td3.appendChild(td3Text);

      let td4 = document.createElement('td');
      let td4Text = document.createTextNode("Total Price");
      td4.appendChild(td4Text);

      let td5 = document.createElement('td');
      let td5Text = document.createTextNode("Order date");
      td5.appendChild(td5Text);

      
      headTr.appendChild(td1);
      headTr.appendChild(td2);
      headTr.appendChild(td3);
      headTr.appendChild(td4);
      headTr.appendChild(td5);


      thead.appendChild(headTr);
 
       let data = JSON.parse(this.response);
       const len = data.length;
       let doShoppingButton;
       let doShoppingButtonTextNode;
      if(len>0)
      {
       for(let i=0;i<len;i++)
       {
          
        let tbodyTr = document.createElement("tr");
        let td1 = document.createElement("td");
        let td2 = document.createElement("td");
        let td3 = document.createElement("td");
        let td4 = document.createElement("td");
        let td5 = document.createElement("td");
        

           let td1TextNode = document.createTextNode(data[i].productId);
           let td2TextNode = document.createTextNode(data[i].quantity);
           let td3TextNode = document.createTextNode(data[i].price);
           let td4TextNode = document.createTextNode(data[i].totalPrice);
           let td5TextNode = document.createTextNode(data[i].orderDate);

       let shoppingButton = document.createElement("button");
       let shoppingButtonTextNode = document.createTextNode("Its time to Shop ");
       
       shoppingButton.appendChild(shoppingButtonTextNode);
       //arrow functions in ES6 feature
       shoppingButton.addEventListener('click',() =>
       {
           window.location.assign("items.html");
        
       })

    

           td1.appendChild(td1TextNode);
           td2.appendChild(td2TextNode);
           td3.appendChild(td3TextNode);
           td4.appendChild(td4TextNode);
           td5.appendChild(td5TextNode);

           tbodyTr.appendChild(td1);
           tbodyTr.appendChild(td2);
           tbodyTr.appendChild(td3);
           tbodyTr.appendChild(td4);
           tbodyTr.appendChild(td5);

           tbody.appendChild(tbodyTr);
         

       }
      }
      else{
         doShoppingButton = document.createElement("button");
         doShoppingButtonTextNode = document.createTextNode("Its time to Shop ");
         
        doShoppingButton.appendChild(doShoppingButtonTextNode);
        //arrow function in ES6 feature to check the orders placed earlier
        doShoppingButton.addEventListener('click',() => 
        {
            window.location.assign("items.html");
         
        })
            let data = document.createElement("h2");
            let noData = document.createTextNode("No Previous Orders Available");
            data.appendChild(noData);
            tbody.appendChild(data);
            
            
      }
       
      table.appendChild(thead);
      table.appendChild(tbody);
      

      let body = document.getElementsByTagName('body')[0];
      
      body.appendChild(ordersHeading);
      body.appendChild(table);

      if(len>0)
      body.appendChild(shoppingButton);
      if(len<1)
      body.appendChild(doShoppingButton);
  }
}
let email = sessionStorage.getItem('${user}');

console.log(email);

 /* api call to users */
httpReq.open('get','http://localhost:3000/orders?userId=${email}'); //string literal
httpReq.send();
}
