cartlist();
//It is used to displays products added to the cart
 function cartlist()
{

    if(sessionStorage.length<2)
    {
        alert("Dont have any products-Please add to cart");
        history.go(-1);
    }
    let table = document.createElement('table');
    table.setAttribute("id",'dataTable');
    
    let thead = document.createElement('thead');
    let tbody = document.createElement('tbody');

    let headTr = document.createElement('tr');

    let td1 = document.createElement('td');
    let td1Text = document.createTextNode("Product Name");
    td1.appendChild(td1Text);

    let td2 = document.createElement('td');
    let td2Text = document.createTextNode("Description");
    td2.appendChild(td2Text);

    let td3 = document.createElement('td');
    let td3Text = document.createTextNode("Price");
    td3.appendChild(td3Text); 

    let td4 = document.createElement('td');
    let td4Text = document.createTextNode("Quantity");
    td4.appendChild(td4Text);

    headTr.appendChild(td1);
    headTr.appendChild(td2);
    headTr.appendChild(td3);
    headTr.appendChild(td4);


    thead.appendChild(headTr);

    for(let i=0; i<sessionStorage.length; i++) 
    {
        let key = sessionStorage.key(i);
       //string literal
        if(key!=='${user}')
        {
            console.log(key);
            let jsonobj = sessionStorage.getItem(key);
            console.log(jsonobj);
            let data = JSON.parse(jsonobj);
            console.log(data.id);
            console.log(data.productName);
            console.log(data.price);
            console.log(data.description);
            console.log(data.quantity);

            let tbodyTr = document.createElement("tr");
            let td1 = document.createElement("td");
            let td2 = document.createElement("td");
            let td3 = document.createElement("td");
            let td4 = document.createElement("td");

            let td1TextNode = document.createTextNode(data.productName);
            let td2TextNode = document.createTextNode(data.description);
            let td3TextNode = document.createTextNode(data.price);
            let td4TextNode = document.createTextNode(data.quantity);

            let orderButton = document.createElement("button");
            let orderButtonTextNode = document.createTextNode("Place Order");
            orderButton.appendChild(orderButtonTextNode);
            //ES6 arrow function is used here to  check the orders
            orderButton.addEventListener('click',() =>  
            {
                window.location.assign("orders.html");
            })

            let backButton = document.createElement("button");
            let backButtonTextNode = document.createTextNode("Back");
            backButton.appendChild(backButtonTextNode);
            backButton.addEventListener('click',() => //arrow function
            {
                history.go(-1);
            })
            

            td1.appendChild(td1TextNode);
            td2.appendChild(td2TextNode);
            td3.appendChild(td3TextNode);
            td4.appendChild(td4TextNode);

            tbodyTr.appendChild(td1);
            tbodyTr.appendChild(td2);
            tbodyTr.appendChild(td3);
            tbodyTr.appendChild(td4);
 
            tbody.appendChild(tbodyTr);

            table.appendChild(thead);
            table.appendChild(tbody);
            
      
            let body = document.getElementsByTagName('body')[0];

            body.appendChild(table);

        }      

    }
    body.appendChild(orderButton);
    body.appendChild(backButton);
}
