loadProducts();
//this method loads the products which are available for shopping at EBay
function loadProducts()
{
    let httpReq;
    if(window.XMLHttpRequest)
    {
        httpReq = new XMLHttpRequest();
    }
    else{
         httpReq  = new ActiveXObject();
    }
    // api call response handling 
    httpReq.onreadystatechange = function()
    {
  if(this.readyState===4 && this.status===200)
  {
   
    let productsHeading =  document.createElement("h2");
    productsHeading.appendChild(document.createTextNode("Products in EBay "));
    productsHeading.setAttribute("id",'headingID');

      let table = document.createElement('table');
      table.setAttribute("id",'dataTable');
      
      let thead = document.createElement('thead');
      let tbody = document.createElement('tbody');

      let headTr = document.createElement('tr');

      let td1 = document.createElement('td');
      let td1Text = document.createTextNode("Product ID");
      td1.appendChild(td1Text);

      let td2 = document.createElement('td');
      let td2Text = document.createTextNode("Product Name");
      td2.appendChild(td2Text);

      let td3 = document.createElement('td');
      let td3Text = document.createTextNode("Price");
      td3.appendChild(td3Text);

      let td4 = document.createElement('td');
      let td4Text = document.createTextNode("Description");
      td4.appendChild(td4Text);

      let td5 = document.createElement('td');
      let td5Text = document.createTextNode("Rating");
      td5.appendChild(td5Text);

      let td6 = document.createElement('td');
      let td6Text = document.createTextNode("Quantity");
      td6.appendChild(td6Text);

      let td7 = document.createElement('td');
      let td7Text = document.createTextNode("Action");
      td7.appendChild(td7Text);

      
      headTr.appendChild(td1);
      headTr.appendChild(td2);
      headTr.appendChild(td3);
      headTr.appendChild(td4);
      headTr.appendChild(td5);
      headTr.appendChild(td6);
      headTr.appendChild(td7);

      thead.appendChild(headTr);
 
       let data = JSON.parse(this.response);
       const len = data.length;
      if(len>0)
      {
       for(let i=0;i<len;i++)
       {
           
           let tbodyTr = document.createElement("tr");
           let td1 = document.createElement("td");
           let td2 = document.createElement("td");
           let td3 = document.createElement("td");
           let td4 = document.createElement("td");
           let td5 = document.createElement("td");
           let td6 = document.createElement("td");
           let td7 = document.createElement("td");

           let td1TextNode = document.createTextNode(data[i].id);
           let td2TextNode = document.createTextNode(data[i].productName);
           let td3TextNode = document.createTextNode(data[i].price);
           let td4TextNode = document.createTextNode(data[i].description);
           let td5TextNode = document.createTextNode(data[i].rating);
           let td6InputElement = document.createElement("input");
           td6InputElement.setAttribute("type","number");
           td6InputElement.setAttribute("min",1);
           td6InputElement.setAttribute("id","qty"+i);

       let addToCartButton = document.createElement("button");
       let addToCartButtonTextNode = document.createTextNode("Add to cart");
       addToCartButton.appendChild(addToCartButtonTextNode);
       addToCartButton.addEventListener('click', function()
       {
           let data = this.parentElement.parentElement.cells;
           console.log(data);
         let qty = data[5].childNodes[0].value;
         if(qty=="")
         {
           alert("Specify the count to Add the products");
           return;
        }
        if(qty==0)
         {
           alert("Add to Shop products of minimum one");
           return;
        }
           let obj1 = {id:data[0].innerHTML,productName:data[1].innerHTML,price:data[2].innerHTML,description:data[3].innerHTML,rating:data[4].innerHTML,quantity:qty};
           sessionStorage.setItem(data[0].innerHTML,JSON.stringify(obj1));
        
       })
       var orderButton = document.createElement("button");
       var orderButtonTextNode = document.createTextNode("Show cart");
       orderButton.appendChild(orderButtonTextNode);
       orderButton.addEventListener('click', function()
       {
           window.location.assign("cart.html");
       })
    

           td1.appendChild(td1TextNode);
           td2.appendChild(td2TextNode);
           td3.appendChild(td3TextNode);
           td4.appendChild(td4TextNode);
           td5.appendChild(td5TextNode);
           td6.appendChild(td6InputElement);
           td7.appendChild(addToCartButton);
       

           tbodyTr.appendChild(td1);
           tbodyTr.appendChild(td2);
           tbodyTr.appendChild(td3);
           tbodyTr.appendChild(td4);
           tbodyTr.appendChild(td5);
           tbodyTr.appendChild(td6);
           tbodyTr.appendChild(td7);

           tbody.appendChild(tbodyTr);
         

       }
      }
      else{
            let data = document.createElement("h4");
            let noData = document.createTextNode("Data not Available");
            data.appendChild(noData);
            tbody.appendChild(data);
      }
       
      table.appendChild(thead);
      table.appendChild(tbody);
      

      let body = document.getElementsByTagName('body')[0];
      
      body.appendChild(productsHeading);
      body.appendChild(table);
      body.appendChild(orderButton);
  }
}
 // api call to products
httpReq.open('GET','http://localhost:3000/products',true);
httpReq.send();
}
