//valid user login using arrow function
 login = () =>
{
    const email = document.getElementById("email").value;
    const password = document.getElementById("password").value;
//its used to check the validation for users who login with registered details
    if((email==="" || email===null)||(password==="" || password===null)){
        alert("Enter  Email and password to login the application");
        return false;       
    }
//shorthand properties
    let obj={
      email,password};
    
        let httpRequest;
    
        if (window.XMLHttpRequest) {
        httpRequest = new XMLHttpRequest();
          }
        else {
        httpRequest = new ActiveXObject();
          }
/* 
//promise
   httpRequest.onreadystatechange = function () {
     return new Promise(function  (resolve,reject) {
      if(this.readyState ===4){
      if(this.status!=200){   
        reject("Error code" + this.status)
      
      }else
      {
      window.alert("Logged In successfully");
console.log(this.response);
window.location.assign("shoppingitems.html");
      }
     }

    }
     */

        // api call response handling 
        httpRequest.onreadystatechange = function ()  {
        if (this.status === 200 && this.readyState === 4) {
        window.alert("Logged In successfully");
        window.location.assign("items.html");
            }
          }
 

        /* converting obj to json to send over api call */
        jsonUser = JSON.stringify(obj);
    
    console.log(jsonUser);
    
        /* api call to obj */
        httpRequest.open('get','http://localhost:3000/customers?email=${email}&password=${password}');
        httpRequest.setRequestHeader('Content-type', 'application/json; charset=utf-8');
        httpRequest.send(jsonUser);
        }




