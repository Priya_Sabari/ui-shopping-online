//used to add user shopping using arrow function
 register =()=> 
 
{
  
    const username =  document.getElementById("userName").value;
    const mobile = document.getElementById("mobile").value;
    const email = document.getElementById("email").value;
    const pwd = document.getElementById("password").value;
    const address  = document.getElementById("address").value;

    const pattern="[A-Za-z0-9._%+-]*(@dbs.com|@hcl.com)";
//its checking the validations of the user details while new user registers

    if(username===""){
    alert("Please Enter username"); 
    return false;       
    }

    else if(mobile===""){
    alert("Please Enter Mobile");
    return false;       
    }
    else if(mobile.length!=10){
    alert("Please Enter 10 digit Mobile Number");
    return false;       
    }
    else if(email===""){
    alert("Please Enter Email");
    return false;       
    }
//it checks the user email is getting matched to the given pattern
    else if(!email.match(pattern)){
    alert("You have to enter either DBS/HCL mail id to register");
    return false;       
    }
    else if (pwd ==="") {
        alert("Please enter a password.");
        return false;
    }
   
    else if(address===""){
        alert("Please Enter address");
        return false;       
    }
    else if(address.length<10){
        alert("Please add detailed address");
        return false;       
    }
   //shorthand properties of the user
    let user = {
    username,
    mobile,
    email,
    pwd,
    address:address
    }

      
    let httpRequest;

    if (window.XMLHttpRequest) {
    httpRequest = new XMLHttpRequest();
      }
    else {
    httpRequest = new ActiveXObject();
      }
    // api call response handling 
    httpRequest.onreadystatechange = function() {
    if (this.status === 201 && this.readyState === 4) {
    window.alert("user added, please login!");
    window.location.assign("login.html");
        }
      }
    /* converting user to json to send over api call */
    jsonUser = JSON.stringify(user);

console.log(jsonUser);

    /* api call to users */
    const url='http://localhost:3000/customers';
    httpRequest.open('POST',url,true);
    httpRequest.setRequestHeader('Content-type', 'application/json; charset=utf-8');
    httpRequest.send(jsonUser);

    }
    